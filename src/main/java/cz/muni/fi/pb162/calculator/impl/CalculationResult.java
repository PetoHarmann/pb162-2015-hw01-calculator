package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.*;

/**
 * @author Peter Harmann 433311
 * @version 31.10.2015
 */

public class CalculationResult implements Result {
    private boolean success;
    private boolean alphanumeric;
    private double numericValue;
    private String alphanumericValue;

    CalculationResult(double numericValue) {
        this.success = true;
        this.alphanumeric = false;
        this.numericValue = numericValue;
    }

    CalculationResult(boolean success, String alphanumericValue) {
        this.success = success;
        this.alphanumeric = true;
        this.alphanumericValue = alphanumericValue;
    }

    @Override
    public boolean isSuccess() {
        return success;
    }

    @Override
    public boolean isAlphanumeric() {
        return alphanumeric;
    }

    @Override
    public boolean isNumeric() {
        return !alphanumeric;
    }

    @Override
    public double getNumericValue() {
        if(alphanumeric) return Double.NaN;
        return numericValue;
    }

    @Override
    public String getAlphanumericValue() {
        return alphanumericValue;
    }
}
