package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.*;

/**
 * @author Peter Harmann 433311
 * @version 31.10.2015
 */

public class AdvancedCalculator extends BasicCalculator implements ConvertingCalculator {
    private int value(char ch) {
        for(int i=0; i<16; ++i) if(DIGITS.charAt(i) == ch) return i;
        return -1;
    }

    @Override
    public Result toDec(int base, String number) {
        if(base <= 1 || base > 16) return new CalculationResult(false, COMPUTATION_ERROR_MSG);
        int result = 0;
        for(int i = (number.charAt(0) == '-' ? 1 : 0), len = number.length(); i<len; ++i) {
            int val = value(number.charAt(i));
            if(val == -1) return new CalculationResult(false, COMPUTATION_ERROR_MSG);
            result = result*base + val;
        }
        if(number.charAt(0) == '-') result = -result;
        return new CalculationResult(result);
    }

    @Override
    public Result fromDec(int base, int number) {
        if(base <= 1 || base > 16) return new CalculationResult(false, COMPUTATION_ERROR_MSG);
        String str = "";
        int temp = number;
        if(number < 0) {
            temp = -number;
        }
        while(temp != 0) {
            str = DIGITS.charAt(temp % base) + str;
            temp /= base;
        }
        if(number < 0) {
            str = "-" + str;
        }
        return new CalculationResult(true, str);
    }

    public Result eval(String input) {
        String[] ops = input.split(" ");
        switch(ops[0]) {
            case TO_DEC_CMD:
                if(ops.length != 3) return new CalculationResult(false, WRONG_ARGUMENTS_ERROR_MSG);
                return toDec(Integer.parseInt(ops[1]), ops[2]);
            case FROM_DEC_CMD:
                if(ops.length != 3) return new CalculationResult(false, WRONG_ARGUMENTS_ERROR_MSG);
                return fromDec(Integer.parseInt(ops[1]), Integer.parseInt(ops[2]));
            default:
                return super.eval(input);
        }
    }
}
