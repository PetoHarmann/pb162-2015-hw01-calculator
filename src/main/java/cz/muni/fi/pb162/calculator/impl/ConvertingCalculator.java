package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.*;

/**
 * @author Peter Harmann 433311
 * @version 31.10.2015
 */

public interface ConvertingCalculator extends Calculator, NumeralConverter {

}
