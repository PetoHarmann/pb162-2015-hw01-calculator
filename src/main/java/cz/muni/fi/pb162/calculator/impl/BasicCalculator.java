package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Calculator;
import cz.muni.fi.pb162.calculator.Result;

/**
 * @author Peter Harmann 433311
 * @version 31.10.2015
 */

public class BasicCalculator implements Calculator {
    @Override
    public Result eval(String input) {  // Split for Interface
        String[] ops = input.split(" ");
        return eval(ops);
    }

    public Result eval(String[] ops) { // Using ops for better override in AdvancedCalculator (avoiding double split)
        switch(ops[0]) {
            case SUM_CMD:
                if(ops.length != 3) return new CalculationResult(false, WRONG_ARGUMENTS_ERROR_MSG);
                return sum(Double.parseDouble(ops[1]), Double.parseDouble(ops[2]));
            case SUB_CMD:
                if(ops.length != 3) return new CalculationResult(false, WRONG_ARGUMENTS_ERROR_MSG);
                return sub(Double.parseDouble(ops[1]), Double.parseDouble(ops[2]));
            case MUL_CMD:
                if(ops.length != 3) return new CalculationResult(false, WRONG_ARGUMENTS_ERROR_MSG);
                return mul(Double.parseDouble(ops[1]), Double.parseDouble(ops[2]));
            case DIV_CMD:
                if(ops.length != 3) return new CalculationResult(false, WRONG_ARGUMENTS_ERROR_MSG);
                return div(Double.parseDouble(ops[1]), Double.parseDouble(ops[2]));
            case FAC_CMD:
                if(ops.length != 2) return new CalculationResult(false, WRONG_ARGUMENTS_ERROR_MSG);
                return fac(Integer.parseInt(ops[1]));
            default:
                return new CalculationResult(false, UNKNOWN_OPERATION_ERROR_MSG);
        }
    }

    @Override
    public Result sum(double x, double y) {
        return new CalculationResult(x+y);
    }

    @Override
    public Result sub(double x, double y) {
        return new CalculationResult(x-y);
    }

    @Override
    public Result mul(double x, double y) {
        return new CalculationResult(x*y);
    }

    @Override
    public Result div(double x, double y) {
        if(y == 0) return new CalculationResult(false, COMPUTATION_ERROR_MSG);
        return new CalculationResult(x/y);
    }

    @Override
    public Result fac(int x) {
        if(x < 0) return new CalculationResult(false, COMPUTATION_ERROR_MSG);
        double result = 1;
        for(int i=2; i<=x; ++i) result *= i;
        return new CalculationResult(result);
    }
}
